console.log("Mabuhay!");

// JS - is loosely type Programming Language

alert("Hello Again");

console. log ( "Hello World!" );

console.
log
(
	"Hello World!"
);

//[SECTION] - Making Comments in JS

/*	
	1. Single-Line Comment: Ctrl + /
	2. Multi-Line Comment: Ctrl + Shift + /
*/

// [SECTION] - Variables
// used to contain data
// usually stored in a computer's memory

let myVariable;

console.log(myVariable);

let mySecondVariable = 2;

console.log(mySecondVariable);

let productName = "desktop computer"
console.log(productName);

// Reassigning value
let friend;
friend = "Kate";
friend = "Jane";
friend = "Donny";
console.log(friend);

// Syntax for const
const pet = "Bruno";
// pet = "Lala";
console.log(pet);

// Local and Global Variables

let outerVariable = "hello"; //sample global variable

{
	let innerVariable = "Hello World!" //local variable
	console.log(innerVariable);
}

console.log(outerVariable);

const outerExample = "Global Variable";

{
	const innerExample ="Local variable";
	console.log(innerExample);
}

console.log(outerExample);

// Multiple Declaration
let productCode = "DC017", productBrand ="Dell";

/*let productCode = "DC017";
let productBrand = "Dell";
*/


console.log(productCode, productBrand);


// DATA TYPES

// Strings
let country = 'Philippines';
let province = "Metro Manila";

	//Contatenating strings
	let fullAddress = province + ", " + country;
	console.log(fullAddress);

	let	greeting = "I live in" + ", " + country;
	console.log(greeting);

	// Escape characters (\)
	let mailAddress = "Metro Manila\n\nPhilippines";
	console.log(mailAddress);

	// Double quote- ginagamit para pwede gumamit ng single quote sa loob ng text
	let	message = "John's employees went home early";
	console.log(message);

// Numbers
let headcount = 26;
console.log(headcount);
	// Decimal number or Fractions
	let	grade = 98.7;
	console.log(grade);

	// Exponential Notation
	let planetDistance = 2e10;
	console.log(planetDistance);

	// Combining String and Integer
	console.log("My grade last sem is " + grade);

	// Boolean
	let	isMarried = false;
	let inGoodConduct = true;
	console.log("isMarried: " + isMarried); 
	console.log("inGoodConduct: " + inGoodConduct);

	// Arrays (pwedeng same data types, pwede ding hindi)
	let grades = [98.7, 92.1, 90.2, 94.6];
	console.log(grades);

	let details = ["John", "Smith", 32, true];
	console.log(details);

	// Objects -  parang same na sa Real world objects
	let person = {
		fullName: "Juan Dela Cruz",
		age: 35,
		isMarried: false,
		contact: ["09123456789", "09999999999"],
		// Nested Object
		address: {
			houseNumber: "345",
			city: "Manila"
		}
	}

	console.log(person);

	// Re-assign value to Array
	const anime =["one piece", "one punch man", "Attack on Titan"];
	anime[1]="Kimetsu no Yaiba";

	console.log(anime);

